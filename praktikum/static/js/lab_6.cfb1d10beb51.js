// Chatbox
var chathead = document.getElementsByClassName('chat-head');
var chatbody = document.getElementsByClassName('chat-body');

$(chathead).click(function(){
    $(".chat-body").slideToggle('Fast');
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
  	print.value = null;
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END


$(document).ready(function() {
    $('.my-select').select2();
});

theme = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]

$('.my-select').select2({
    'data': JSON.parse(**ISI DENGAN DATA THEMES DARI LOCAL STORAGE**)
})

$('.apply-button-class').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select

    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada

    // [TODO] ambil object theme yang dipilih

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya

    // [TODO] simpan object theme tadi ke local storage selectedTheme
})