theme = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]

var backgroundColor;
var textColor;
var index;

// Fungsi ganti tema
function changeTheme(bColor, tColor){
  $('body').css({'backgroundColor': bColor});
  $('h1').css({'color': tColor});
}

if (localStorage.getItem('index') === null) {
  index = 3;
} else {
  index = JSON.parse(localStorage.getItem('index'));
}
backgroundColor = theme[index]['bcgColor'];
textColor = theme[index]['fontColor'];
console.log(theme[index]['bcgColor']);
changeTheme(backgroundColor, textColor);

$(document).ready(function(){
    $('.my-select').select2({
      'data' : theme
    });
    $('.my-select').val(index).change();
    $('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    index = $('.my-select').val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    backgroundColor = theme[index]['bcgColor'];
    textColor = theme[index]['fontColor'];
    // [TODO] ambil object theme yang dipilih
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    changeTheme(backgroundColor, textColor);
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    localStorage.setItem('index', JSON.stringify(index));
  })
});


// Chatbox
var chathead = document.getElementsByClassName('chat-head');
var chatbody = document.getElementsByClassName('chat-body');

$(chathead).click(function(){
    $(".chat-body").slideToggle('Fast');
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = null;
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'log') {
      print.value = Math.log10(print.value);
  } else if (x === 'sin') {
      print.value = Math.sin(print.value);
  } else if (x === 'tan') {
      print.value = Math.tan(print.value);
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

var sender = true;

$('textarea').keypress(function(e){
  if(e.keyCode == 13 && !e.shiftKey) {
    var c = String.fromCharCode(e.which);
    var textValue = $('textarea').val();
    var fulltext = textValue + c;
    e.preventDefault();
    $('textarea').val('');

    if(sender){
      $('.msg-insert').append('<div class = "msg-send">' + fulltext + '</div>');
      sender = false;
    } else {
      $('.msg-insert').append('<div class = "msg-receive">' + fulltext + '</div>');
      sender = true;
    }
  }
});